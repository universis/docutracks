

export const DocumentReferenceKind = {
    Default: {
        Id: 1
    },
}

export const DocumentReferenceType = {
    Incoming: {
        Id: 1
    },
    Outgoing: {
        Id: 2
    },
}

export declare interface DocumentCopy {
    CreatedByGroup: ObjectReference,
    OwnedByGroup: ObjectReference
}

export declare interface DocumentReference {
    MainFile?: DocumentMainFile;
    Attachments: DocumentAttachment[];
    Title: string;
    CreatedBy?: ObjectReference;
    CreatedByGroup: ObjectReference;
    CreatedForGroup: ObjectReference;
    Kind: ObjectReference;
    Type: ObjectReference;
    Comments?: string;
    DocumentCopies: DocumentCopy[];
}

export declare interface DocumentAttachment {
    FileName: string;
    Base64File: string;
}

export interface ObjectReference {
    Id: number;
}


export interface DocumentMainFile {
    FileName: string;
    Base64File: string;
    IsEncrypted: boolean;
}

export interface OrganizationGroup {
    Id: number;
    DisplayName: string;
    SubGroups: OrganizationGroup[]
}

export interface OrganizationGroupItem {
    id: number;
    name: string;
    alternateName: string;
    path: string;
}