import { ApplicationService, IApplication } from '@themost/common';
import { DataPermissionEventListener, DataQueryable, EdmMapping, EdmType, FunctionConfiguration, ModelClassLoaderStrategy, ODataModelBuilder, PermissionMask, ProcedureConfiguration, SchemaLoaderStrategy } from '@themost/data';
import { DocumentNumberService } from '@universis/docnumbers';
import { OrganizationGroupItem } from './DocutracksNumberService.interfaces';
import {promisify} from 'util';
declare interface GetGroupsService {
    getGroups(): Promise<OrganizationGroupItem>
}

declare interface ODataModelBuilderCleanup {
    clean(all: boolean): void;
    initializeSync(): void;
}

@EdmMapping.entityType('DocumentNumberSeries')
class DepartmentDocumentNumberSeries {

    @EdmMapping.func('Groups',EdmType.CollectionOf('Object'))
    static async getGroups(context: any) {
        // validate permissions
        const validator = new DataPermissionEventListener();
        // noinspection JSUnresolvedFunction
        const validateAsync = promisify(validator.validate);
        // get first user department
        const user = await context.model('User').where('name').equal(context.user.name).getTypedItem();
        const userDepartments: DataQueryable = user && user.property('departments');
        let firstUserDepartment: { id: any } = null;
        if (userDepartments) {
            firstUserDepartment = await userDepartments.select('id').getItem();
        }
        // validate create permissions
        await validateAsync({
            model: context.model('DepartmentDocumentNumberSeries'),
            mask: PermissionMask.Create,
            target: {
                department: firstUserDepartment
            },
        } as any);
        const service: GetGroupsService = context.getApplication().getService(DocumentNumberService);
        if (typeof service.getGroups === 'function') {
            return await service.getGroups();
        }
    }

}

export class DepartmentDocumentNumberSeriesReplacer extends ApplicationService {
    constructor(app: any) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader: SchemaLoaderStrategy = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy as any);
        // get model definition
        const model = schemaLoader.getModelDefinition('DepartmentDocumentNumberSeries');
        // get model class
        const loader: ModelClassLoaderStrategy = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy as any);
        const DepartmentDocumentNumberSeriesBase: any = loader.resolve(model);
        // extend class
        DepartmentDocumentNumberSeriesBase.getGroups = DepartmentDocumentNumberSeries.getGroups;
        const builder: ODataModelBuilder = this.getApplication().getStrategy(ODataModelBuilder as any);
        if (builder != null) {
            builder.addEntity('OrganizationGroupItem')
                .addProperty('id', EdmType.EdmInt32, false)
                .addProperty('name', EdmType.EdmString, false)
                .addProperty('alternateName', EdmType.EdmString, false)
                .addProperty('path', EdmType.EdmString, false);
            builder.getEntity('DepartmentDocumentNumberSeries').collection
                .addFunction('Groups').returnsCollection('OrganizationGroupItem');
        }
    }
}