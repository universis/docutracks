## @universis/docutracks

Implementation of document numbering services hosted by docutracks https://www.docutracks.eu/

### Installation

    npm i @universis/docutracks

### Configuration

Register `DocutracksNumberService` as application services:

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/docnumbers#DocumentNumberService",
            "strategyType": "@universis/docutracks#DocutracksNumberService"
        }
    ]
