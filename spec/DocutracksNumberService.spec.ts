import { ExpressDataApplication, ExpressDataContext } from '@themost/express';
import { DocutracksNumberService } from '../src/index';
import path from 'path';
import { DocumentNumberSeries } from '@universis/docnumbers';
import { RandomUtils } from '@themost/common';
describe('DocutracksNumberService', () => {
    let app: any;
    let context: ExpressDataContext;
    beforeAll(() => {
        app = new ExpressDataApplication(path.resolve(__dirname, 'app/config'));
        // set testing configuration
        app.getConfiguration().setSourceAt('settings/universis/docutracks', {
            server: process.env.DOCUTRACKS_SERVER,
            user: process.env.DOCUTRACKS_USER || process.env.DOCUTRACKS_USERNAME,
            password: process.env.DOCUTRACKS_PASSWORD
        });
        context = app.createContext();
    });
    it('should create instance', () => {
        const service = new DocutracksNumberService(app);
        expect(service).toBeTruthy();
    });
    it('should get next number', async () => {
        const model = context.model('DocumentNumberSeries');
        expect(model).toBeTruthy();
        const documentSeries: any = {
            name: 'Document Series #1',
            alternateName: '/Groups/104',
            active: true,
            numberFormat: '{0}'
        }
        await model.silent().save(documentSeries);
        expect(documentSeries.id).toBeTruthy();

        const service = new DocutracksNumberService(app);
        expect(service).toBeTruthy();
        const nextNumber = await service.next(context, documentSeries);
        expect(nextNumber).toBeTruthy();
    });

    it('should get next number async', async () => {
        const model = context.model('DocumentNumberSeries');
        expect(model).toBeTruthy();
        const documentSeries: any = {
            name: 'Document Series #1',
            alternateName: '/Groups/104',
            active: true,
            numberFormat: '{0}'
        }
        await model.silent().save(documentSeries);
        expect(documentSeries.id).toBeTruthy();
        const service = new DocutracksNumberService(app);
        expect(service).toBeTruthy();
        const sources = [];
        for (let index = 0; index < 5; index++) {
            sources.push(new Promise<any>((resolve, reject) => {
                setTimeout(() => {
                    service.next(context, documentSeries).then((result) => {
                        if (result == null) {
                            return reject(new Error('Not Found'))
                        }
                        return resolve(result);
                    }).catch((err) => {
                        return reject(err);
                    });
                }, RandomUtils.randomInt(1000, 1200));
            }));
        }
        const results = await Promise.all(sources);
        for (const result of results) {
            expect(result).toBeTruthy();
        }
    });

    it('should get groups', async () => {
        const service = new DocutracksNumberService(app);
        const sources = [];
        for (let index = 0; index < 10; index++) {
            sources.push(new Promise<any>((resolve, reject) => {
                setTimeout(() => {
                    service.getGroups().then((result) => {
                        if (result == null) {
                            return reject(new Error('Not Found'))
                        }
                        return resolve(result);
                    }).catch((err) => {
                        return reject(err);
                    });
                }, RandomUtils.randomInt(3000, 4000));
            }));
        }
        const results = await Promise.all(sources);
        for (const result of results) {
            expect(result).toBeTruthy();
        }
    });

    it('should get user async', async () => {
        const service = new DocutracksNumberService(app);
        const sources = [];
        for (let index = 0; index < 10; index++) {
            sources.push(new Promise<any>((resolve, reject) => {
                setTimeout(() => {
                    service.getUser(process.env.DOCUTRACKS_FIND_USER).then((result) => {
                        if (result.User == null) {
                            return reject(new Error('User Not Found'))
                        }
                        return resolve(result.User);
                    }).catch((err) => {
                        return reject(err);
                    });
                }, RandomUtils.randomInt(1000, 2000));
            }));
        }
        const results = await Promise.all(sources);
        for (const result of results) {
            expect(result).toBeTruthy();
        }
    });

});